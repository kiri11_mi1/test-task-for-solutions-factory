from django.db import models


class Mailing(models.Model):
    # TODO:
    # добавить поле filters

    start_sending = models.DateTimeField('Запуск рассылки')
    end_sending = models.DateTimeField('Конец рассылки')
    text = models.TextField(verbose_name='Текст сообщения')

    def __str__(self):
        return f'Рассылка{self.id}: {self.text[:15]}'

    class Meta:
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'
