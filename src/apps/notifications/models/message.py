from django.db import models

from ..constants import MessageSendingStatuses


class Message(models.Model):
    created_at = models.DateTimeField('Дата и время создания')
    status_sending = models.CharField(
        verbose_name='Статус отправки',
        max_length=8,
        choices=MessageSendingStatuses.CHOICES,
    )
    mailing = models.ForeignKey(
        to='notifications.Mailing',
        on_delete=models.CASCADE,
        related_name='messages',
        verbose_name='Рассылка',
    )
    user = models.ForeignKey(
        to='users.User',
        on_delete=models.CASCADE,
        related_name='messages',
        verbose_name='Пользователь',
    )

    def __str__(self):
        return f'Message {self.id}'

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'
