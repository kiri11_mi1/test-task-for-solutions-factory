from typing import Final


class MessageSendingStatuses:
    SUCCESS: Final[str] = 'success'
    FAILURE: Final[str] = 'failure'

    CHOICES: Final[tuple[tuple[str, str], ...]] = (
        (SUCCESS, 'Успешно'),
        (FAILURE, 'Неудачно'),
    )

    CHOICES_DICT: Final[dict[str, str]] = dict(CHOICES)
